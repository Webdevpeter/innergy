import { pricingHistory } from "./consts/pricingHistory";
import { filterParentlessChildServices } from "./helpers/filterParentlessChildServices";
import { getDiscountAmount } from "./helpers/getDiscountAmount";

export type ServiceYear = 2020 | 2021 | 2022;
export type ServiceType = "Photography" | "VideoRecording" | "BlurayPackage" | "TwoDayEvent" | "WeddingSession";


// I'm not sure what did you mean by "reducer-like style" so I just used a switch (as I would in case of writing a reducer)
// and returned a copy of the previous state (which I assumed was previouslySelectedServices) to ensure the state doesn't get mutated
export const updateSelectedServices = (
    previouslySelectedServices: ServiceType[],
    action: { type: "Select" | "Deselect"; service: ServiceType }
) => {
  const { type, service } = action;
  switch (type) {
    case "Select":
      return previouslySelectedServices.includes(service)
        ? filterParentlessChildServices([...previouslySelectedServices])
        : filterParentlessChildServices([...previouslySelectedServices, service]);
    case "Deselect":
      return filterParentlessChildServices(previouslySelectedServices.filter(selectedService => selectedService !== service));
    default:
      return filterParentlessChildServices([...previouslySelectedServices]);
  }
};

export const calculatePrice = (selectedServices: ServiceType[], selectedYear: ServiceYear) => {
  const discountAmount = getDiscountAmount(selectedServices, selectedYear);
  const prices = pricingHistory[selectedYear];
  const basePrice = selectedServices.reduce((sum, service) => sum + prices[service], 0);
  const finalPrice = basePrice - discountAmount;

  return { basePrice, finalPrice }
};