import { ServiceType } from "..";
import { servicesConfig } from "../consts/servicesConfig";

export const filterParentlessChildServices = (selectedServices: ServiceType[]) => selectedServices.filter(selectedService => {
  const serviceConfig = servicesConfig[selectedService];
  if (!serviceConfig.parents) {
    return true;
  }

  return serviceConfig.parents.some(parent => selectedServices.includes(parent));
});