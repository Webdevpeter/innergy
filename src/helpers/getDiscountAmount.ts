import { ServiceType, ServiceYear } from "..";
import { getAllApplicableDiscounts } from "./getAllApplicableDiscounts";


export const getDiscountAmount = (selectedServices: ServiceType[], selectedYear: ServiceYear) => {
  const allApplicableDiscounts = getAllApplicableDiscounts(selectedServices, selectedYear);

  return allApplicableDiscounts.reduce((sum, discount) => sum + discount.amount, 0);
}