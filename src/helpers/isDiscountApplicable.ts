import { ServiceType } from "..";
import { Discount } from "../consts/discounts";

export const isDiscountApplicable = (selectedServices: ServiceType[], discount: Discount) => {
  return discount.requiredServices.reduce((isApplicable, requiredService) => isApplicable && selectedServices.includes(requiredService), true);
};
