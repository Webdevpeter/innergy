import { ServiceType, ServiceYear } from "..";
import { Discount, discounts } from "../consts/discounts";
import { isDiscountApplicable } from "./isDiscountApplicable";

const hasCollidingTriggerServices = (discountOne: Discount, discountTwo: Discount) => {
  return discountOne.triggerServices.some(triggerOne => {
    return discountTwo.triggerServices.some(triggerTwo => triggerOne === triggerTwo)
  })
};

const filterLowerDiscounts = (discounts: Discount[], filterDiscount: Discount) => {
  return discounts.filter(discount => {
    if (!hasCollidingTriggerServices(discount, filterDiscount)) {
      return true;
    }

    return discount.amount >= filterDiscount.amount;
  });
}

export const getAllApplicableDiscounts = (selectedServices: ServiceType[], selectedYear: ServiceYear) => {
  return discounts[selectedYear].reduce((applicableDiscounts, discount) => {
    if (!isDiscountApplicable(selectedServices, discount)) {
      return applicableDiscounts;
    }

    const filteredDiscounts = filterLowerDiscounts(applicableDiscounts, discount);
    const hasCollidingTriggers = applicableDiscounts.reduce((hasColliding, applicableDiscount) => hasColliding || hasCollidingTriggerServices(applicableDiscount, discount), false)

    if (hasCollidingTriggers && filteredDiscounts.length === applicableDiscounts.length) {
      return applicableDiscounts;
    }

    return [...applicableDiscounts, discount];
  }, [] as Discount[]);
}