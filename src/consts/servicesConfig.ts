import { ServiceType } from "..";

type ServiceConfig = {
  name: ServiceType;
  parents?: ServiceType[];
}
type ServicesConfig = { [Property in ServiceType]: ServiceConfig };

export const servicesConfig: ServicesConfig = {
  Photography: { name: "Photography" },
  VideoRecording: { name: "VideoRecording" },
  BlurayPackage: { name: "BlurayPackage", parents: ["VideoRecording"] },
  TwoDayEvent: { name: "TwoDayEvent", parents: ["Photography", "VideoRecording"] },
  WeddingSession: { name: "TwoDayEvent" },
};