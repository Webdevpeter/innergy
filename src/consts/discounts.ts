import { ServiceType, ServiceYear } from "..";

export type Discount = {
  requiredServices: ServiceType[];
  triggerServices: ServiceType[];
  amount: number;
};

export type Discounts = { [Property in ServiceYear]: Discount[] }

export const discounts: Discounts = {
  "2020": [
    {
      requiredServices: ['Photography', 'VideoRecording'],
      triggerServices: ['Photography', 'VideoRecording'],
      amount: 1200,
    },
    {
      requiredServices: ['WeddingSession', 'Photography'],
      triggerServices: ['WeddingSession'],
      amount: 300,
    },
    {
      requiredServices: ['WeddingSession', 'VideoRecording'],
      triggerServices: ['WeddingSession'],
      amount: 300,
    },
  ],
  "2021": [
    {
      requiredServices: ['Photography', 'VideoRecording'],
      triggerServices: ['Photography', 'VideoRecording'],
      amount: 1300,
    },
    {
      requiredServices: ['WeddingSession', 'Photography'],
      triggerServices: ['WeddingSession'],
      amount: 300,
    },
    {
      requiredServices: ['WeddingSession', 'VideoRecording'],
      triggerServices: ['WeddingSession'],
      amount: 300,
    },
  ],
  "2022": [
    {
      requiredServices: ['Photography', 'VideoRecording'],
      triggerServices: ['Photography', 'VideoRecording'],
      amount: 1300,
    },
    {
      requiredServices: ['WeddingSession', 'Photography'],
      triggerServices: ['WeddingSession'],
      amount: 600,
    },
    {
      requiredServices: ['WeddingSession', 'VideoRecording'],
      triggerServices: ['WeddingSession'],
      amount: 300,
    },
  ],
}