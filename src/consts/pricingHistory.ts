import { ServiceType, ServiceYear } from "..";

type Pricing = { [Property in ServiceType]: number };
type PricingHistory = { [Property in ServiceYear]: Pricing };

export const pricingHistory: PricingHistory = {
  "2020": {
    Photography: 1700,
    VideoRecording: 1700,
    WeddingSession: 600,
    BlurayPackage: 300,
    TwoDayEvent: 400,
  },
  "2021": {
    Photography: 1800,
    VideoRecording: 1800,
    WeddingSession: 600,
    BlurayPackage: 300,
    TwoDayEvent: 400,
  },
  "2022": {
    Photography: 1900,
    VideoRecording: 1900,
    WeddingSession: 600,
    BlurayPackage: 300,
    TwoDayEvent: 400,
  }
};